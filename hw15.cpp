// hw15.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>

void Func(int param, int n)
{
    for (int i = 0; i <= n; ++i)
    {
        if (i % 2 == 1 && param == 1) std::cout << i << " ";
        else if (i % 2 == 0 && param == 2) std::cout << i << " ";
    }
}

int main()
{
    int n = 10;
    for (int i = 0; i <= n; ++i)
    {
        if (i % 2 == 0) std::cout << i << " ";
    }
    std::cout << std::endl;
    int p = 2;
    Func(p, 20);

}

